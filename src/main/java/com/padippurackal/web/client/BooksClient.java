package com.padippurackal.web.client;

import com.padippurackal.web.service.model.BookRequest;
import com.padippurackal.web.service.model.BookResponse;
import com.padippurackal.web.service.model.BooksPort;
import com.padippurackal.web.service.model.BooksPortService;

public class BooksClient {
    public static void main(String[] args) {

        BooksPortService booksPortService = new BooksPortService();
        BooksPort booksPort = booksPortService.getBooksPortSoap11();

        BookRequest bookRequest = new BookRequest();
        bookRequest.setName("Supernatural");

        BookResponse bookResponse = booksPort.book(bookRequest);
        System.out.println(bookResponse.getBook().getAuthor());
    }
}
